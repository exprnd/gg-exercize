from coding_gg import get_numb_xor, get_numb_naive

# Test cases
test_list1 = [1]
test_list2 = [2, 3, 3, 2, 2]
test_list3 = [2, 3, 3, 2, 2, 2, 4]
test_list4 = [5,5,5,5,5,5,6]
test_list5 = [-5,-5,4,4,5,5,130]

sol_test1 = 1
sol_test2 = 2
sol_test3 = 4
sol_test4 = 6
sol_test5 = 130


# Testing get_numb_naive
def get_numb_naive_tests():
    assert(sol_test1 == get_numb_naive(test_list1))
    assert(sol_test2 == get_numb_naive(test_list2))
    assert(sol_test3 == get_numb_naive(test_list3))
    assert(sol_test4 == get_numb_naive(test_list4))
    assert(sol_test5 == get_numb_naive(test_list5))
    print("All tests get_numb_naive passed")

def get_numb_xor_tests():
    # Testing get_numb_xor
    assert(sol_test1 == get_numb_xor(test_list1))
    assert(sol_test2 == get_numb_xor(test_list2))
    assert(sol_test3 == get_numb_xor(test_list3))
    assert(sol_test4 == get_numb_xor(test_list4))
    assert(sol_test5 == get_numb_xor(test_list5))
    print("All tests get_numb_xor passed")    
    

if __name__ == "__main__":
    get_numb_naive_tests()
    get_numb_xor_tests()