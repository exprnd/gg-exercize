# README

## Install prerequisites
To run the tests we need python3 installed on your system. If you are not sure that it is available please run the following (only for Unix systems):

`. ./installation.sh`

## Tests
To run the tests execute:

`python3 coding_gg_test.py`

## Complexity
A note on the complexity of the two algorithms is available in the latex build `complexity_gg.pdf`.
