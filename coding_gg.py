def get_numb_naive(list_of_numbers):
    numbers_dictionary = dict()
    for n in list_of_numbers:
        n_string = str(n)
        if (numbers_dictionary.get(n_string) == None):
            numbers_dictionary[n_string] = True
        else:
            numbers_dictionary[n_string] = not(numbers_dictionary.get(n_string))
    
    for n in numbers_dictionary:
        if (numbers_dictionary.get(n) == True):
            return int(n)

def get_numb_xor(list_of_numbers):
    sum = 0
    for n in list_of_numbers:
        sum = sum^n
    return sum
