#!/bin/bash
if [[ "$OSTYPE" == "linux-gnu" ]]; then
    sudo apt-get update
    sudo apt-get install python3
elif [[ "$OSTYPE" == "darwin"* ]]; then
    which -s python3
    if [[ $? != 0 ]] ; then
        which -s brew
        if [[ $? != 0 ]] ; then
            # Install Homebrew
            ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
        else
            brew ls --versions python3
            if [[ $? != 0 ]] ; then
                # installed
            else
                brew install python3
            fi
        fi
    fi
else
    echo "ERROR: OS system not supported from this script."
    echo "Please install manually Python3 and related dependecies."
fi
